import * as cors from 'cors';
import * as express from 'express';
import * as expressWS from 'express-ws';
import * as ws from 'ws';
const { createCanvas } = require('canvas');
const HISTORY_LIMIT = 5;

function handleAction(canvasId: number, action: CanvasAction) {
  const targetCanvas = canvasData.get(canvasId) as CanvasData;
  let drawingPrimitive = targetCanvas.drawingPrimitive as Primitive;
  switch (action.type) {
    case 'startPrimitive':
      switch (action.payload['type']) {
        case 'path':
        case 'eraser':
          targetCanvas.drawingPrimitive = {
            type: 'path',
            color: action.payload['color'],
            width: action.payload['width'],
            vertices: [{ x: action.payload['x'], y: action.payload['y'] }]
          };
          break;

        default:
          break;
      }
      break;
    case 'addVertex':
      switch (drawingPrimitive.type) {
        case 'path':
        case 'eraser':
          const vertices = drawingPrimitive.vertices as Vertex[];
          vertices.push(action.payload);
          break;

        default:
          break;
      }
      break;

    case 'clearCanvas':
      targetCanvas.drawingPrimitive = { type: 'clear' };
    case 'endPrimitive':
      targetCanvas.queuedPrimitive = targetCanvas.queuedPrimitive.slice(
        0,
        targetCanvas.historyCount
      );
      targetCanvas.queuedPrimitive.push(
        targetCanvas.drawingPrimitive as Primitive
      );
      targetCanvas.historyCount = targetCanvas.queuedPrimitive.length;
      while (targetCanvas.historyCount > HISTORY_LIMIT) {
        const primitive = targetCanvas.queuedPrimitive.shift();
        drawPrimitive(targetCanvas.canvas, primitive as Primitive);
        --targetCanvas.historyCount;
      }
      targetCanvas.drawingPrimitive = null;
      break;
    case 'setHistory':
      targetCanvas.historyCount = action.payload.history;
      break;
    default:
      break;
  }
}

function drawPrimitive(canvas: HTMLCanvasElement, primitive: Primitive) {
  if (!primitive) {
    return;
  }
  const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
  switch (primitive.type) {
    case 'path':
    case 'eraser':
      const vertices = primitive.vertices as Vertex[];
      const firstV = vertices[0];
      ctx.strokeStyle = primitive.color as string;
      ctx.lineWidth = primitive.width as number;
      ctx.lineCap = 'round';
      ctx.beginPath();
      ctx.moveTo(firstV.x, firstV.y);
      for (let i = 1; i < vertices.length - 1; ++i) {
        const v1 = vertices[i];
        const v2 = vertices[i + 1];
        const mid: Vertex = {
          x: Math.floor((v1.x + v2.x) / 2),
          y: Math.floor((v1.y + v2.y) / 2)
        };
        ctx.quadraticCurveTo(v1.x, v1.y, mid.x, mid.y);
      }
      const lastV = vertices[vertices.length - 1];
      ctx.lineTo(lastV.x, lastV.y);
      ctx.lineTo(lastV.x + 0.01, lastV.y + 0.01);
      ctx.stroke();
      break;

    case 'clear':
      ctx.fillStyle = 'white';
      ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
      break;

    default:
      break;
  }
}

interface Vertex {
  x: number;
  y: number;
}

interface Primitive {
  type: string;
  color?: string;
  width?: number;
  vertices?: Vertex[];
}

interface CanvasData {
  title: string;

  owner: string;
  painter: string;

  canvas: HTMLCanvasElement;
  ctx: CanvasRenderingContext2D;

  historyCount: number;
  drawingPrimitive: Primitive | null;
  queuedPrimitive: Primitive[];

  sessions: Set<SessionData>;
}

interface UserData {
  name: string;
  account: string;
  password: string;
}

interface SessionData extends UserData {
  socket: ws;
}

interface CanvasAction {
  type: string;
  payload?: any;
}

const app = express();
expressWS(app);

const router = express.Router() as expressWS.Router;
const canvasData = new Map<number, CanvasData>();
const userData = new Map<string, UserData>();

function new_canvas_id(): number {
  while (true) {
    const canvasId = Math.floor(1e3 * Math.random());
    if (!canvasData.has(canvasId)) {
      return canvasId;
    }
  }
}

function login(account: string, password: string): boolean {
  const data = userData.get(account) as UserData;
  if (data && data.password === password) {
    return true;
  }
  return false;
}

function pushUserList(canvasId: number) {
  const targetCanvas = canvasData.get(canvasId) as CanvasData;
  const users: { name: string; account: string }[] = [];

  for (let sessions of targetCanvas.sessions) {
    users.push({ name: sessions.name, account: sessions.account });
  }

  for (let session of targetCanvas.sessions) {
    session.socket.send(
      JSON.stringify({
        type: 'setUserList',
        payload: { users }
      })
    );
  }
}

function pushPrivilege(canvasId: number) {
  const targetCanvas = canvasData.get(canvasId) as CanvasData;

  for (let session of targetCanvas.sessions) {
    session.socket.send(
      JSON.stringify({
        type: 'setPrivilege',
        payload: {
          owner: targetCanvas.owner,
          painter: targetCanvas.painter
        }
      })
    );
  }
}

app.use(cors());

router.ws('/canvas/:canvasId', (ws, req) => {
  const canvasId: number = +req.params.canvasId;
  if (!canvasData.has(canvasId)) {
    const action: CanvasAction = {
      type: 'error',
      payload: { message: 'The canvas does not exist.' }
    };
    ws.send(JSON.stringify(action));
    ws.close();
    return;
  }

  let currentSession: SessionData;
  let currentCanvas: CanvasData;

  ws.on('message', message => {
    const action: CanvasAction = JSON.parse(message as string);
    switch (action.type) {
      case 'auth':
        if (currentSession == null) {
          const { account, password } = action.payload;
          if (login(account, password)) {
            const user = userData.get(account) as UserData;
            currentSession = {
              ...user,
              socket: ws
            };
            currentCanvas = canvasData.get(canvasId) as CanvasData;
            currentCanvas.sessions.add(currentSession);
            ws.send(JSON.stringify({ type: 'authSuccess' }));
            pushUserList(canvasId);
            ws.send(
              JSON.stringify({
                type: 'setPrivilege',
                payload: {
                  owner: currentCanvas.owner,
                  painter: currentCanvas.painter
                }
              })
            );
            ws.send(
              JSON.stringify({
                type: 'loadCanvas',
                payload: {
                  title: currentCanvas.title,
                  drawingPrimitive: currentCanvas.drawingPrimitive,
                  queuedPrimitive: currentCanvas.queuedPrimitive,
                  historyImage: currentCanvas.canvas.toDataURL(),
                  historyCount: currentCanvas.historyCount
                }
              })
            );
            break;
          }
        }
        ws.send(JSON.stringify({ type: 'authFailed' }));
        break;
      case 'setPainter':
        if (currentCanvas.owner === currentSession.account) {
          currentCanvas.painter = action.payload.painter;
        }
        pushPrivilege(canvasId);
        break;
      case 'setTitle':
        if (currentCanvas.owner === currentSession.account) {
          currentCanvas.title = action.payload.title;
          for (let session_i of currentCanvas.sessions) {
            if (session_i !== currentSession) {
              session_i.socket.send(message);
            }
          }
        }
        break;
      default:
        if (currentSession) {
          for (let session_i of currentCanvas.sessions) {
            if (session_i !== currentSession) {
              session_i.socket.send(message);
            }
          }
          handleAction(canvasId, action);
        } else {
          ws.send(
            JSON.stringify({
              type: 'error',
              payload: { message: 'Authentication required.' }
            })
          );
        }
        break;
    }
  });

  ws.on('close', () => {
    if (currentSession) {
      currentCanvas.sessions.delete(currentSession);
      pushUserList(canvasId);
    }
  });
});

router.post('/canvas', express.json(), (req, res) => {
  const { account, password } = req.body;
  let action: CanvasAction;
  if (login(account, password)) {
    const canvasId = new_canvas_id();
    const canvas = createCanvas(1200, 1600) as HTMLCanvasElement;
    canvasData.set(canvasId, {
      title: '',
      owner: account,
      painter: account,
      historyCount: 0,
      canvas: canvas,
      ctx: canvas.getContext('2d') as CanvasRenderingContext2D,
      sessions: new Set<SessionData>(),
      drawingPrimitive: null,
      queuedPrimitive: []
    });
    action = {
      type: 'success',
      payload: { canvasId }
    };
  } else {
    action = {
      type: 'error',
      payload: { message: 'Authentication required.' }
    };
  }
  res.json(action);
});

router.post('/signup', express.json(), (req, res) => {
  const { name, account, password } = req.body;
  let action: CanvasAction;
  if (name && account && password) {
    if (userData.has(account)) {
      action = {
        type: 'error',
        payload: {
          message: 'Account already exists.'
        }
      };
    } else {
      userData.set(account, { name, account, password });
      action = {
        type: 'success',
        payload: {
          message: 'Signup success, you are now logged in.'
        }
      };
    }
  } else {
    action = {
      type: 'error',
      payload: { message: 'Data missing' }
    };
  }
  res.json(action);
});

router.post('/login', express.json(), (req, res) => {
  let action: CanvasAction;
  const { account, password } = req.body;
  const user = userData.get(account) as UserData;
  if (login(account, password)) {
    action = {
      type: 'success',
      payload: { name: user.name }
    };
  } else {
    action = {
      type: 'error',
      payload: { message: 'Account or password incorrect.' }
    };
  }
  res.json(action);
});

app.use('/', router);

app.listen(8888);
